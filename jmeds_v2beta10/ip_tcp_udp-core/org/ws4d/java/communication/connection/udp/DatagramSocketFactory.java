/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 ******************************************************************************/
package org.ws4d.java.communication.connection.udp;

import java.io.IOException;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.NetworkInterface;
import org.ws4d.java.communication.filter.AddressFilter;
import org.ws4d.java.util.Clazz;
import org.ws4d.java.util.Log;

/**
 * Creates server and client sockets.
 */
public abstract class DatagramSocketFactory {

	private static DatagramSocketFactory	instance	= null;

	public static synchronized DatagramSocketFactory getInstance() {
		if (instance == null) {
			try {
				Class clazz = Clazz.forName("org.ws4d.java.communication.connection.udp.PlatformDatagramSocketFactory");
				instance = (DatagramSocketFactory) clazz.newInstance();
			} catch (Exception e) {
				Log.error("Unable to create PlatformDatagramSocketFactory: " + e.getMessage());
				throw new RuntimeException(e.getMessage());
			}
		}
		return instance;
	}

	/**
	 * Creates a datagramm server socket for given port.
	 * 
	 * @param remotePort
	 * @return datagramm socket
	 * @throws IOException
	 */
	public abstract DatagramSocket createDatagramServerSocket(int localPort, AddressFilter filter) throws IOException;

	/**
	 * Creates a datagramm server socket for given address, port and interface.
	 * 
	 * @param localAddress
	 * @param localPort
	 * @param iface
	 * @return datagramm socket
	 * @throws IOException
	 */
	public abstract DatagramSocket createDatagramServerSocket(IPAddress localAddress, int localPort, NetworkInterface iface, AddressFilter filter) throws IOException;

	public abstract String getJavaVersion();
}
