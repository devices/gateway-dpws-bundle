/*
 * File: DPWSGateTest.java
 * Project: org.unirostock.bagateway.dpws
 * @author Robert Balla
 */


import org.unirostock.bagateway.dpws.DPWSGateImpl;
import org.unirostock.bagateway.entitiy.RootEntity;
import org.unirostock.bagateway.interfaces.GateInterface;

// TODO: Auto-generated Javadoc
/**
 * The Class DPWSGateTest.
 */
public class DPWSGateTest
{
	
	/** The gate. */
	static GateInterface gate;
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String [ ] args)
	{
		gate = new DPWSGateImpl();
		gate.openGate();
		
		//newDevice()-Test
		//RootEntity ent = new RootEntity("BACnet", "test device", null);
		//gate.newDevice(ent);
	}
}
