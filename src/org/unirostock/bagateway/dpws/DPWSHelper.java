package org.unirostock.bagateway.dpws;

import org.unirostock.bagateway.dpws.tables.UnitTable;
import org.unirostock.bagateway.enums.GWdatatype;
import org.unirostock.bagateway.enums.GWunit;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;

public class DPWSHelper
{
	public static String adaptString(String str)
	{
		while (str.contains(" "))
		{
			String[] substrings = str.split(" ");
			
			if (substrings.length > 1)
			{
				for (int i = 1; i < substrings.length; i++)
				{
					String c = "" + substrings[i].charAt(0);
					c = c.toUpperCase();
					substrings[i] = c + substrings[i].substring(1);
				}
			}
			
			str = "";
			for (String s : substrings)
			{
				str = str + s;
			}
		}
		
		return str;
	}
	
	public static String getDefaultValue(GWdatatype type)
	{
		if (type == null)
			return "";
		
		if (type.equals(GWdatatype.Integer))
		{
			return "0";
		} else if (type.equals(GWdatatype.CharacterString))
		{
			return "";
		} else if (type.equals(GWdatatype.Unit))
		{
			return UnitTable.gw2dpws.get(GWunit.Celsius);
		}
		
		return "";
	}
}
