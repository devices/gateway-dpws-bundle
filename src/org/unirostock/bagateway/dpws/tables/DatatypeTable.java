package org.unirostock.bagateway.dpws.tables;

import java.util.HashMap;

import org.unirostock.bagateway.enums.GWdatatype;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;

public class DatatypeTable
{
	public static final HashMap<Type, GWdatatype> dpws2gw;
	public static final HashMap<GWdatatype, Type> gw2dpws;
	
	static
	{
		dpws2gw = new HashMap<Type, GWdatatype>();
		gw2dpws = new HashMap<GWdatatype, Type>();
		
		dpws2gw.put(SchemaUtil.TYPE_BOOLEAN, GWdatatype.Boolean);
		dpws2gw.put(SchemaUtil.TYPE_BYTE, GWdatatype.Byte);
		dpws2gw.put(SchemaUtil.TYPE_UNSIGNED_BYTE, GWdatatype.Byte);
		dpws2gw.put(SchemaUtil.TYPE_STRING, GWdatatype.CharacterString);
		dpws2gw.put(SchemaUtil.TYPE_NORMALIZED_STRING, GWdatatype.CharacterString);
		dpws2gw.put(SchemaUtil.TYPE_TOKEN, GWdatatype.CharacterString);
		dpws2gw.put(SchemaUtil.TYPE_DOUBLE, GWdatatype.Double);
		dpws2gw.put(SchemaUtil.TYPE_FLOAT, GWdatatype.Float);
		dpws2gw.put(SchemaUtil.TYPE_INT, GWdatatype.Integer);
		dpws2gw.put(SchemaUtil.TYPE_INTEGER, GWdatatype.Integer);
		dpws2gw.put(SchemaUtil.TYPE_NON_POSITIVE_INTEGER, GWdatatype.Integer);
		dpws2gw.put(SchemaUtil.TYPE_NEGATIVE_INTEGER, GWdatatype.Integer);
		dpws2gw.put(SchemaUtil.TYPE_UNSIGNED_INT, GWdatatype.UnsignedInteger);
		dpws2gw.put(SchemaUtil.TYPE_NON_NEGATIVE_INTEGER, GWdatatype.UnsignedInteger);
		dpws2gw.put(SchemaUtil.TYPE_POSITIVE_INTEGER, GWdatatype.UnsignedInteger);
		
		gw2dpws.put(GWdatatype.Boolean, SchemaUtil.TYPE_BOOLEAN);
		gw2dpws.put(GWdatatype.Byte, SchemaUtil.TYPE_BYTE);
		gw2dpws.put(GWdatatype.CharacterString, SchemaUtil.TYPE_STRING);
		gw2dpws.put(GWdatatype.Double, SchemaUtil.TYPE_DOUBLE);
		gw2dpws.put(GWdatatype.Float, SchemaUtil.TYPE_FLOAT);
		gw2dpws.put(GWdatatype.Integer, SchemaUtil.TYPE_INT);
		gw2dpws.put(GWdatatype.UnsignedInteger, SchemaUtil.TYPE_UNSIGNED_INT);	
	}
}
