package org.unirostock.bagateway.dpws.tables;

import java.util.HashMap;

import org.unirostock.bagateway.enums.GWunit;

public class UnitTable
{
	public static final HashMap<String, GWunit> dpws2gw;
	public static final HashMap<GWunit, String> gw2dpws;
	
	static
	{
		dpws2gw = new HashMap<String, GWunit>();
		gw2dpws = new HashMap<GWunit, String>();
		
		dpws2gw.put("%", GWunit.Percent);	
		dpws2gw.put("C", GWunit.Celsius);
		dpws2gw.put("F", GWunit.Fahrenheit);
		dpws2gw.put("K", GWunit.Kelvin);
		
		gw2dpws.put(GWunit.Percent, "%");
		gw2dpws.put(GWunit.Celsius, "C");
		gw2dpws.put(GWunit.Fahrenheit, "F");
		gw2dpws.put(GWunit.Kelvin, "K");
		
	}
}
