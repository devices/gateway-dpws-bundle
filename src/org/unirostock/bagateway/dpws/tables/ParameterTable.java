package org.unirostock.bagateway.dpws.tables;

import java.util.HashMap;

import org.unirostock.bagateway.enums.GWparameter;

public class ParameterTable
{
	public static final HashMap<String, GWparameter> dpws2gw;
//	public static final HashMap<GWparameter, String> gw2dpws;
	
	static
	{
		dpws2gw = new HashMap<String, GWparameter>();
//		gw2dpws = new HashMap<GWparameter, String>();
		
		dpws2gw.put("value", GWparameter.PRESENT_VALUE);
		dpws2gw.put("maxvalue", GWparameter.PRESVAL_LIMIT_MAX);
		dpws2gw.put("minvalue", GWparameter.PRESVAL_LIMIT_MIN);
		dpws2gw.put("status", GWparameter.STATUS);
		dpws2gw.put("unit", GWparameter.PRESVAL_UNIT);
	}
}
