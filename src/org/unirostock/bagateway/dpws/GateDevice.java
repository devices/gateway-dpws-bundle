/*
 * File: DPWSGateDevice.java
 * Project: org.uniRostock.bacnetDPWS.dpwsGate
 * @author Robert Balla
 */
package org.unirostock.bagateway.dpws;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Enumeration;
import java.util.logging.Logger;

import org.unirostock.bagateway.interfaces.GatewayInterface;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.ConnectionInfo;
import org.ws4d.java.constants.MessageConstants;
import org.ws4d.java.dispatch.AllMessageSelector;
import org.ws4d.java.dispatch.MessageInformer;
import org.ws4d.java.dispatch.listener.MessageListener;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.service.Device;

import sun.net.util.IPAddressUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class DPWSGateDevice.
 */
public class GateDevice extends DefaultDevice
{
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(GateDevice.class.getName());
	
	
	
	/**
	 * Instantiates a new DPWS gate device.
	 *
	 * @param comManId the com man id
	 */
	public GateDevice(String comManId)
	{
		super(comManId);
		
		this.setPortTypes(new QNameSet(new QName("BACnetDPWSGateway", DPWSGateImpl.NAMESPACE)));
		this.addFriendlyName(LocalizedString.LANGUAGE_DE, "BACnet-Gateway f�r eingebettete Web-Services");
		this.addFriendlyName(LocalizedString.LANGUAGE_EN, "BACnet-Gateway for embedded Web-Services");
		this.addManufacturer(LocalizedString.LANGUAGE_DE, "Uni Rostock");
		this.addManufacturer(LocalizedString.LANGUAGE_EN, "Uni Rostock");
	}
}
