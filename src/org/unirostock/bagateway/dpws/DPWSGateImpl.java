/*
 * File: DPWSGateImpl.java
 * Project: org.uniRostock.bacnetDPWS.dpwsGate
 * @author Robert Balla
 */
package org.unirostock.bagateway.dpws;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.unirostock.bagateway.dpws.operation.ForeignEvent;
import org.unirostock.bagateway.dpws.service.GateService;
import org.unirostock.bagateway.dpws.tables.ParameterTable;
import org.unirostock.bagateway.dpws.tables.DatatypeTable;
import org.unirostock.bagateway.dpws.tables.UnitTable;
import org.unirostock.bagateway.entitiy.GatewayInternalEntity;
import org.unirostock.bagateway.entitiy.ParameterEntity;
import org.unirostock.bagateway.entitiy.RootEntity;
import org.unirostock.bagateway.entitiy.SubEntity;
import org.unirostock.bagateway.enums.GWdatatype;
import org.unirostock.bagateway.enums.GWparameter;
import org.unirostock.bagateway.enums.GWunit;
import org.unirostock.bagateway.interfaces.GateInterface;
import org.unirostock.bagateway.interfaces.GatewayInterface;
import org.ws4d.java.CoreFramework;
import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.ConnectionInfo;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.constants.MessageConstants;
import org.ws4d.java.constants.DPWS2011.DPWSConstants2011;
import org.ws4d.java.dispatch.AllMessageSelector;
import org.ws4d.java.dispatch.MessageInformer;
import org.ws4d.java.dispatch.listener.MessageListener;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.EventSource;
import org.ws4d.java.eventing.EventingException;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.DefaultEventSource;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.ListIterator;
//import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.HelloData;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class DPWSGateImpl.
 */
public class DPWSGateImpl implements GateInterface
{
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(GateDevice.class.getName());
	
	/** The Constant PROTOCOL. */
	public static final String PROTOCOL = "DPWS";
	
	/** The Constant NAMESPACE. */
	public static final String NAMESPACE = "http://unirostock.org/gateway";
	
	/** The Constant PROBE_RESPONSE_TIME. */
	private static final Integer PROBE_RESPONSE_TIME = 5000;
	
	/** The com man. */
	CommunicationManager comMan;
	
	/** The dpws gate device. */
	GateDevice	gateDevice;
	
	/** The dpws gate service. */
	GateService gateService;
	
	/** The dpws gate client. */
	final public GateClient gateClient;
	
	/** The msg listener. */
	DPWSMessageListener msgListener;
	
	/** The gateway. */
	private GatewayInterface gateway;
	
	/** The no dpws devices. */
	final Hashtable<AttributedURI ,ForeignDevice> foreignDevices;
	
	/** The probe reqs. */
	final LinkedList<ProbeRequest> probeReqs;

	/**
	 * Instantiates a new DPWS gate impl.
	 */
	public DPWSGateImpl()
	{
		this.makeLogFile();
		
		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		CoreFramework.start(null);
		
		foreignDevices	= new Hashtable<AttributedURI ,ForeignDevice>();
		probeReqs		= new LinkedList<ProbeRequest>();
		
		gateDevice		= new GateDevice(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		gateService	= new GateService(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		gateDevice.addService(gateService);
		
		gateClient = new GateClient();
		
		comMan = CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		msgListener = new DPWSMessageListener();
	}
	
	/**
	 * Sets the gateway.
	 *
	 * @param service the new gateway
	 */
	public void setGateway(GatewayInterface service)
	{
		gateway = service;
	}
	
	/**
	 * Gets the gateway.
	 *
	 * @return the gateway
	 */
	public GatewayInterface getGateway()
	{
		return gateway;
	}

	/* (non-Javadoc)
	 * @see org.unirostock.bacnetdpws.GateInterface#openGate()
	 */
	@Override
	public void openGate()
	{
		try
		{
			MessageInformer.getInstance().addMessageListener(msgListener, new AllMessageSelector());
			gateClient.registerHelloListening();
			
			gateDevice.start();
		} catch (IOException e)
		{
			log.warning("Couldn't start the Gate-Device!");
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bacnetdpws.GateInterface#closeGate()
	 */
	@Override
	public void closeGate()
	{
		try
		{
			gateDevice.sendBye();
			gateDevice.stop();
			CoreFramework.stop();
		} catch (IOException e)
		{
			log.warning("Couldn't stop the Gate-Device!");
			e.printStackTrace();
		}
		
	}
	

	/* (non-Javadoc)
	 * @see org.unirostock.bagateway.interfaces.GateInterface#newDevice(org.unirostock.bagateway.entitiy.RootEntity)
	 */
	@Override
	public RootEntity newDevice(RootEntity dev)
	{
		ForeignDevice device = new ForeignDevice(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, dev, gateway);
		foreignDevices.put(device.getEndpointReference().getAddress(), device);
		
		try
		{
			device.start();
			
			for (ProbeRequest req : probeReqs)
			{
				if ((System.currentTimeMillis() - req.time_ms) > PROBE_RESPONSE_TIME)
				{
					probeReqs.remove(req);
				}
				else
				{
					Long diff = System.currentTimeMillis() - req.time_ms;
					if (diff < DPWSConstants2011.DPWS_APP_MAX_DELAY)
					{
						try
						{
							Thread.sleep(diff);
						} catch (InterruptedException e)
						{
							log.warning("Thread.sleep() failed!");
							e.printStackTrace();
						}
					}
					
					device.sendPropbeMatch(req.msg, req.connectInfo, comMan);
				}
			}
			
			
			
		} catch (IOException e)
		{
			log.warning("New device wasn't able to start or send Hello!");
			e.printStackTrace();
		}
		
		dev.addIdentifier(PROTOCOL, device.getEndpointReference().getAddress());
		
		return dev;
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bacnetdpws.GateInterface#announceDev(java.lang.Object)
	 */
	@Override
	public void announceDevice(RootEntity dev)
	{
		ForeignDevice d = foreignDevices.get(dev.getOriginIdentifier());
		
		if (d == null)
			return;
		
		for (ProbeRequest req : probeReqs)
		{
			if ((System.currentTimeMillis() - req.time_ms) > PROBE_RESPONSE_TIME)
			{
				probeReqs.remove(req);
			}
			else
			{
				Long diff = System.currentTimeMillis() - req.time_ms;
				if (diff < DPWSConstants2011.DPWS_APP_MAX_DELAY)
				{
					try
					{
						Thread.sleep(diff);
					} catch (InterruptedException e)
					{
						log.warning("Thread.sleep() failed!");
						e.printStackTrace();
					}
				}
				
				d.sendPropbeMatch(req.msg, req.connectInfo, comMan);
			}
		}
		
		d.sendHello();
		return;
	}

	/* (non-Javadoc)
	 * @see org.unirostock.bacnetdpws.GateInterface#discoverDev(java.lang.String)
	 */
	@Override
	public void discoverDevice(String name)
	{
		gateClient.searchDevice(null);
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bagateway.interfaces.GateInterface#readValue(org.unirostock.bagateway.entitiy.RootEntity, org.unirostock.bagateway.entitiy.GatewayInternalEntity)
	 */
	@Override
	public Hashtable<Object, String> readValue(RootEntity rootEnt, LinkedList<Object> path, Hashtable<Object, String> request)
	{
		try
		{
			return gateClient.readValue(rootEnt, path, request);
		} catch (CommunicationException | AuthorizationException | InvocationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.unirostock.bagateway.interfaces.GateInterface#writeValue(org.unirostock.bagateway.entitiy.RootEntity, org.unirostock.bagateway.entitiy.GatewayInternalEntity, java.lang.String)
	 */
	@Override
	public void writeValue(RootEntity rootEnt, LinkedList<Object> path, Hashtable<Object, String> request)
	{
				
		try
		{
			gateClient.writeValue(rootEnt, path, request);
		} catch (CommunicationException | AuthorizationException | InvocationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void subscribe(RootEntity rootEnt, LinkedList<Object> path, long duration)
	{
		try
		{
			gateClient.subscribe(rootEnt, path, duration);
		} catch (CommunicationException | EventingException | IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void notification(RootEntity rootEnt, LinkedList<Object> path,
			Hashtable<Object, String> value)
	{
		ForeignDevice device = foreignDevices.get(rootEnt.getIdentifier(PROTOCOL));
		SubEntity ent = rootEnt.getSubEntity(path.getLast());
		
		
		ServiceReference serviceRef = null;
		ForeignEvent event = null;
		
		String eventName =  null;
		if (ent.getDirection() != null && ent.getCount() != -1)
		{
			eventName =  DPWSHelper.adaptString(ent.getDirection().toString() + ent.getCount().toString());
		}
		else
		{
			eventName =  DPWSHelper.adaptString(ent.getOriginIdentifier().toString());
		}
		
		String interfaceName = null;
		if (ent.getGroup() != null)
		{
			interfaceName = DPWSHelper.adaptString(ent.getGroup().toString());
		}
		else
		{
			interfaceName =  DPWSHelper.adaptString(ent.getOriginIdentifier().toString());
		}
		
		try
		{
			serviceRef = device.getServiceReference(new URI(DPWSGateImpl.NAMESPACE + "/" + rootEnt.getOriginProtocol() + "/" + rootEnt.getName() + "/" + rootEnt.getName() + "Service"), SecurityKey.EMPTY_KEY);
			event = (ForeignEvent) serviceRef.getService().getEventSource(new QName(interfaceName, DPWSGateImpl.NAMESPACE + "/" + rootEnt.getOriginProtocol() + "/" + rootEnt.getName() + "/" + rootEnt.getName() + "Service") , "Subscribe" + eventName , null, null);
			event.fireForeignEvent(value);
		} catch (CommunicationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * The listener interface for receiving DPWSMessage events.
	 * The class that is interested in processing a DPWSMessage
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addDPWSMessageListener<code> method. When
	 * the DPWSMessage event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see DPWSMessageEvent
	 */
	class DPWSMessageListener implements MessageListener
	{

		/* (non-Javadoc)
		 * @see org.ws4d.java.dispatch.listener.MessageListener#receivedInboundMessage(org.ws4d.java.message.Message, org.ws4d.java.communication.ConnectionInfo, org.ws4d.java.types.AttributedURI)
		 */
		@Override
		public void receivedInboundMessage(Message msg, ConnectionInfo connectionInfo, AttributedURI optionalMessageId)
		{
			if(msg.getType() == MessageConstants.PROBE_MESSAGE)
			{
				log.info("Incoming Probe-Message: " + msg.toString());
				ProbeMessage probeMsg = (ProbeMessage) msg;
				
				probeReqs.add(new ProbeRequest(probeMsg, System.currentTimeMillis(), connectionInfo));
				
				if (gateway != null)
				{
					gateway.discoverDevice(DPWSGateImpl.PROTOCOL, null);
				} else
		   	{
					log.info("No Gateway found!");
		   	}
			}
			
			if(msg.getType() == MessageConstants.PROBE_MATCHES_MESSAGE)
			{
				log.info("Incoming ProbeMatches-Message: " + msg.toString());
				ProbeMatchesMessage pmMsg = (ProbeMatchesMessage) msg;
				
				/*
				 * Don't do anything if it's my ProbeMessage
				 */
				if (pmMsg.getEndpointReference().getAddress().equals(gateDevice.getEndpointReference().getAddress()))
				{
					log.info("Probe-Message rejected (UUID of Gate-Device)");
					return;
				}
				
				if (foreignDevices.containsKey(pmMsg.getEndpointReference().getAddress()))
				{
					log.info("Propbe-Message rejected (UUID of Foreign-Device)");
					return;
				}
				
				
				/*
				 * Make an RootEntity of the device 
				 */
				Device device = null;
				RootEntity deviceEnt = null;
				
				try
				{
					device = gateClient.getDeviceReference(pmMsg.getEndpointReference(), comMan.getCommunicationManagerId()).getDevice();
					deviceEnt = new RootEntity(PROTOCOL, device.getFriendlyName(LocalizedString.LANGUAGE_EN), device.getEndpointReference().getAddress());
				} catch (CommunicationException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (deviceEnt == null)
					return;
					
				/*
				 * Make an SubEntity for every Service
				 */
				org.ws4d.java.structures.Iterator serviceRefs = device.getServiceReferences(gateClient.getDeviceReference(pmMsg.getEndpointReference(), comMan.getCommunicationManagerId()).getSecurityKey());
				while (serviceRefs.hasNext())
				{
					ServiceReference serviceRef = (ServiceReference) serviceRefs.next();
					
					SubEntity serviceEnt = null;
					org.ws4d.java.structures.Iterator operationRefs = null;
					org.ws4d.java.structures.Iterator eventRefs = null;
					try
					{	
						deviceEnt.addSubEntity(serviceRef.getService().getServiceId().getPath(), serviceRef.getService().getServiceId());
						serviceEnt = deviceEnt.getSubEntity(serviceRef.getService().getServiceId());
						operationRefs = serviceRef.getService().getAllOperations();
						eventRefs = serviceRef.getService().getAllEventSources();
					} catch (CommunicationException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					if ((serviceEnt == null) || (operationRefs == null))
						break;
					
					/*
					 * Make an SubEntity for every operation
					 */
					Operation operationRef = null;
					SubEntity operationEnt = null;
					while (operationRefs.hasNext())
					{
						operationRef = (Operation) operationRefs.next();
						
						serviceEnt.addSubEntity(operationRef.getName(), operationRef.getNameQuallified());
						operationEnt = serviceEnt.getSubEntity(operationRef.getNameQuallified());
						
						Element output = operationRef.getOutput();
						Element input = operationRef.getInput();

						if (input != null)
						{		
							if (input.getType().isComplexType())
							{
								operationEnt.addSubEntity(input.getName().getLocalPart(), input.getName());
								new EntityOfComplexType(operationEnt.getSubEntity(input.getName()), (ComplexType) input.getType(), Direction.in, null);
							} 
							else
							{
								operationEnt.addParameter(input.getName().getLocalPart(), input.getName());
								operationEnt.getParameter(input.getName()).setParameterName(ParameterTable.dpws2gw.get(input.getName().getLocalPart()));
								operationEnt.getParameter(input.getName()).setWriteable(true);
								operationEnt.getParameter(input.getName()).setDatatype(DatatypeTable.dpws2gw.get(SchemaUtil.getSchemaType(input.getType().getName())));
								if((ParameterTable.dpws2gw.get(input.getName().getLocalPart()) != null))
									if ((ParameterTable.dpws2gw.get(input.getName().getLocalPart()).equals(GWparameter.PRESVAL_UNIT)))
										operationEnt.getParameter(input.getName()).setDatatype(GWdatatype.Unit);
								
								operationEnt.getParameter(input.getName()).setLastValue(DPWSHelper.getDefaultValue(operationEnt.getParameter(input.getName()).getDatatype()));
							}
						}
						else
						{
							ParameterValue value = null;
							try
							{
								value = operationRef.invoke(operationRef.createInputValue(), CredentialInfo.EMPTY_CREDENTIAL_INFO);
							} catch (AuthorizationException | InvocationException | CommunicationException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							if (output.getType().isComplexType())
							{
								log.info("Add SubEntity (Level 3, Element of complexType) with name: " + output.getName().getLocalPart()
										+ ", id: " + output.getName());
								operationEnt.addSubEntity(output.getName().getLocalPart(), output.getName());
								new EntityOfComplexType(operationEnt.getSubEntity(output.getName()), (ComplexType) output.getType(), Direction.out, value);
							} 
							else
							{
								log.info("Add Parameter (Level 3, Element of simpleType) with name: " + input.getName().getLocalPart()
										+ ", id: " + input.getName());
								operationEnt.addParameter(output.getName().getLocalPart(), output.getName());
								operationEnt.getParameter(output.getName()).setParameterName(ParameterTable.dpws2gw.get(output.getName().getLocalPart()));
								operationEnt.getParameter(output.getName()).setReadable(true);
								operationEnt.getParameter(output.getName()).setDatatype(DatatypeTable.dpws2gw.get(SchemaUtil.getSchemaType(output.getType().getName())));
								if((ParameterTable.dpws2gw.get(output.getName().getLocalPart()) != null))
									if ((ParameterTable.dpws2gw.get(output.getName().getLocalPart()).equals(GWparameter.PRESVAL_UNIT)))
										operationEnt.getParameter(output.getName()).setDatatype(GWdatatype.Unit);
								
								operationEnt.getParameter(output.getName()).setLastValue(ParameterValueManagement.getString(value, output.getName().getLocalPart()));
							}
						}
					}
					
					/*
					 * Make an SubEntity for every eventSource
					 */
					if (eventRefs != null)
					{
						DefaultEventSource eventRef = null;
						SubEntity eventEnt = null;
						while (eventRefs.hasNext())
						{
							eventRef = (DefaultEventSource) eventRefs.next();
							
							serviceEnt.addSubEntity(eventRef.getName(), eventRef.getNameQuallified());
							eventEnt = serviceEnt.getSubEntity(eventRef.getNameQuallified());
							
							Element output = eventRef.getOutput();
							
							if (output != null)
							{				
								if (output.getType().isComplexType())
								{
									eventEnt.addSubEntity(output.getName().getLocalPart(), output.getName());
									new EntityOfComplexType(eventEnt.getSubEntity(output.getName()), (ComplexType) output.getType(), Direction.event, null);
								} 
								else
								{
									eventEnt.addParameter(output.getName().getLocalPart(), output.getName());
									eventEnt.getParameter(output.getName()).setParameterName(ParameterTable.dpws2gw.get(output.getName().getLocalPart()));
									eventEnt.getParameter(output.getName()).setReadable(true);
									eventEnt.getParameter(output.getName()).setDatatype(DatatypeTable.dpws2gw.get(SchemaUtil.getSchemaType(output.getType().getName())));
									if((ParameterTable.dpws2gw.get(output.getName().getLocalPart()) != null))
										if ((ParameterTable.dpws2gw.get(output.getName().getLocalPart()).equals(GWparameter.PRESVAL_UNIT)))
											eventEnt.getParameter(output.getName()).setDatatype(GWdatatype.Unit);

								}
							}
						}
					}
				}	

				if (gateway != null)
				{
					gateway.newDevice(PROTOCOL, deviceEnt);
				} else
		   	{
					log.info("No Gateway found!");
		   	}
			}
		}

		/* (non-Javadoc)
		 * @see org.ws4d.java.dispatch.listener.MessageListener#receivedOutboundMessage(org.ws4d.java.message.Message, org.ws4d.java.communication.ConnectionInfo, org.ws4d.java.types.AttributedURI)
		 */
		@Override
		public void receivedOutboundMessage(Message msg, ConnectionInfo connectionInfo, AttributedURI optionalMessageId)
		{
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/**
	 * The Class ProbeRequest.
	 */
	private class ProbeRequest
	{
		
		/** The msg. */
		ProbeMessage msg;
		
		/** The time_ms. */
		Long time_ms;
		
		/** The connect info. */
		ConnectionInfo connectInfo;
		
		/**
		 * Instantiates a new probe request.
		 *
		 * @param msg the msg
		 * @param time the time
		 * @param connectInfo the connect info
		 */
		public ProbeRequest(ProbeMessage msg, Long time, ConnectionInfo connectInfo)
		{
			this.msg = msg;
			this.time_ms = time;
			this.connectInfo = connectInfo;
		}
	}
	
	/**
	 * The Class DPWSGateClient.
	 */
	public class GateClient extends DefaultClient
	{		
		Hashtable<String, EventSubscription> subscriptions;
		
		/**
		 * Instantiates a new DPWS gate client.
		 */
		public GateClient()
		{
			super();
			
			subscriptions = new Hashtable<String, EventSubscription>();
		}

		/* (non-Javadoc)
		 * @see org.ws4d.java.client.DefaultClient#helloReceived(org.ws4d.java.types.HelloData)
		 */
		@Override
		public void helloReceived(HelloData helloData)
		{
			log.info("Incoming Hello-Message: " + helloData.toString());
			
			/*
			 * Don't do anything if it's my HelloMessage
			 */
			if (helloData.getEndpointReference().getAddress().equals(gateDevice.getEndpointReference().getAddress()))
			{
				log.info("Hello-Message rejected (UUID of Gate-Device)");
				return;
			}
			
			if (foreignDevices.containsKey(helloData.getEndpointReference().getAddress()))
			{
				log.info("Hello-Message rejected (UUID of Foreign-Device)");
				return;
			}
			
			/*
			 * Make an RootEntity of the device 
			 */
			Device device = null;
			RootEntity deviceEnt = null;
			
			try
			{
				device = this.getDeviceReference(helloData).getDevice();

				deviceEnt = new RootEntity(PROTOCOL, device.getFriendlyName(LocalizedString.LANGUAGE_EN), device.getEndpointReference().getAddress());
			} catch (CommunicationException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (deviceEnt == null)
				return;
				
			/*
			 * Make an SubEntity for every Service
			 */
			org.ws4d.java.structures.Iterator serviceRefs = device.getServiceReferences(this.getDeviceReference(helloData).getSecurityKey());
			while (serviceRefs.hasNext())
			{
				ServiceReference serviceRef = (ServiceReference) serviceRefs.next();
				
				SubEntity serviceEnt = null;
				org.ws4d.java.structures.Iterator operationRefs = null;
				org.ws4d.java.structures.Iterator eventRefs = null;
				try
				{	
					deviceEnt.addSubEntity(serviceRef.getService().getServiceId().getPath(), serviceRef.getService().getServiceId());
					serviceEnt = deviceEnt.getSubEntity(serviceRef.getService().getServiceId());
					operationRefs = serviceRef.getService().getAllOperations();
					eventRefs = serviceRef.getService().getAllEventSources();
				} catch (CommunicationException e1)
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if ((serviceEnt == null) || ((operationRefs == null) && (eventRefs == null)))
					break;
				
				/*
				 * Make an SubEntity for every operation
				 */
				if (operationRefs != null)
				{
					Operation operationRef = null;
					SubEntity operationEnt = null;
					while (operationRefs.hasNext())
					{
						operationRef = (Operation) operationRefs.next();
						
						serviceEnt.addSubEntity(operationRef.getName(), operationRef.getNameQuallified());
						operationEnt = serviceEnt.getSubEntity(operationRef.getNameQuallified());
						
						Element output = operationRef.getOutput();
						Element input = operationRef.getInput();
	
						if (input != null)
						{		
							if (input.getType().isComplexType())
							{
								operationEnt.addSubEntity(input.getName().getLocalPart(), input.getName());
								new EntityOfComplexType(operationEnt.getSubEntity(input.getName()), (ComplexType) input.getType(), Direction.in, null);
							} 
							else
							{
								operationEnt.addParameter(input.getName().getLocalPart(), input.getName());
								operationEnt.getParameter(input.getName()).setParameterName(ParameterTable.dpws2gw.get(input.getName().getLocalPart()));
								operationEnt.getParameter(input.getName()).setWriteable(true);
								operationEnt.getParameter(input.getName()).setDatatype(DatatypeTable.dpws2gw.get(SchemaUtil.getSchemaType(input.getType().getName())));
								if((ParameterTable.dpws2gw.get(input.getName().getLocalPart()) != null))
									if ((ParameterTable.dpws2gw.get(input.getName().getLocalPart()).equals(GWparameter.PRESVAL_UNIT)))
										operationEnt.getParameter(input.getName()).setDatatype(GWdatatype.Unit);
								
								operationEnt.getParameter(input.getName()).setLastValue(DPWSHelper.getDefaultValue(operationEnt.getParameter(input.getName()).getDatatype()));
							}
						}
						else if (output != null)
						{
							ParameterValue value = null;
							try
							{
								value = operationRef.invoke(operationRef.createInputValue(), CredentialInfo.EMPTY_CREDENTIAL_INFO);
							} catch (AuthorizationException | InvocationException | CommunicationException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							if (output.getType().isComplexType())
							{
								operationEnt.addSubEntity(output.getName().getLocalPart(), output.getName());
								new EntityOfComplexType(operationEnt.getSubEntity(output.getName()), (ComplexType) output.getType(), Direction.out, value);
							} 
							else
							{
								operationEnt.addParameter(output.getName().getLocalPart(), output.getName());
								operationEnt.getParameter(output.getName()).setParameterName(ParameterTable.dpws2gw.get(output.getName().getLocalPart()));
								operationEnt.getParameter(output.getName()).setReadable(true);
								operationEnt.getParameter(output.getName()).setDatatype(DatatypeTable.dpws2gw.get(SchemaUtil.getSchemaType(output.getType().getName())));
								if((ParameterTable.dpws2gw.get(output.getName().getLocalPart()) != null))
									if ((ParameterTable.dpws2gw.get(output.getName().getLocalPart()).equals(GWparameter.PRESVAL_UNIT)))
										operationEnt.getParameter(output.getName()).setDatatype(GWdatatype.Unit);
								
								operationEnt.getParameter(output.getName()).setLastValue(ParameterValueManagement.getString(value, output.getName().getLocalPart()));
							}
						}
					}
				}
				
				/*
				 * Make an SubEntity for every eventSource
				 */
				if (eventRefs != null)
				{
					DefaultEventSource eventRef = null;
					SubEntity eventEnt = null;
					while (eventRefs.hasNext())
					{
						eventRef = (DefaultEventSource) eventRefs.next();
						
						serviceEnt.addSubEntity(eventRef.getName(), eventRef.getNameQuallified());
						eventEnt = serviceEnt.getSubEntity(eventRef.getNameQuallified());
						
						Element output = eventRef.getOutput();
						
						if (output != null)
						{				
							if (output.getType().isComplexType())
							{
								log.info("Add SubEntity (Level 3, Element of complexType) with name: " + output.getName().getLocalPart()
										+ ", id: " + output.getName());
								eventEnt.addSubEntity(output.getName().getLocalPart(), output.getName());
								new EntityOfComplexType(eventEnt.getSubEntity(output.getName()), (ComplexType) output.getType(), Direction.event, null);
							} 
							else
							{
								eventEnt.addParameter(output.getName().getLocalPart(), output.getName());
								eventEnt.getParameter(output.getName()).setParameterName(ParameterTable.dpws2gw.get(output.getName().getLocalPart()));
								eventEnt.getParameter(output.getName()).setReadable(true);
								eventEnt.getParameter(output.getName()).setDatatype(DatatypeTable.dpws2gw.get(SchemaUtil.getSchemaType(output.getType().getName())));
								if((ParameterTable.dpws2gw.get(output.getName().getLocalPart()) != null))
									if ((ParameterTable.dpws2gw.get(output.getName().getLocalPart()).equals(GWparameter.PRESVAL_UNIT)))
										eventEnt.getParameter(output.getName()).setDatatype(GWdatatype.Unit);

							}
						}
					}
				}
			}	

			if (gateway != null)
			{
				gateway.newDevice(PROTOCOL, deviceEnt);
			} else
	   	{
				log.info("No Gateway found!");
	   	}
		}
		
		@Override
		public ParameterValue eventReceived(ClientSubscription subscription, URI actionURI, ParameterValue parameterValue)
		{
			EventSubscription eventSubscription = subscriptions.get(subscription.getClientSubscriptionId());
			ClientSubscription notificationSub = eventSubscription.notificationSub;
			Hashtable<Object, String> values = new Hashtable<Object, String>();
			
			if (subscription.equals(notificationSub)) {
				if (parameterValue.getType().isComplexType()) {
					ComplexType type = (ComplexType) parameterValue.getType();
					org.ws4d.java.structures.Iterator elements = type.elements();
					Element element = null;
					
					while ( elements.hasNext())
					{
						element = (Element) elements.next();
						GWparameter param = ParameterTable.dpws2gw.get(element.getName().getLocalPart());
						String key = null;
						if (param != null)
							key = param.toString();
						else
							key = element.getName().getLocalPart();
						
						values.put(key,  ParameterValueManagement.getString(parameterValue, element.getName().getLocalPart()));
					}
				}
				
				gateway.notification(eventSubscription.path, values);
				
				return null;
			}
			
			return parameterValue;
		}
	
		public Hashtable<Object, String> readValue(RootEntity rootEnt, LinkedList<Object> path, Hashtable<Object, String> request) throws CommunicationException, AuthorizationException, InvocationException
		{
			System.out.println(rootEnt.getOriginIdentifier().toString());
			AttributedURI uri = (AttributedURI) rootEnt.getOriginIdentifier();
			EndpointReference deviceEpr = new EndpointReference(uri);
			DeviceReference deviceRef = this.getDeviceReference(deviceEpr, comMan.getCommunicationManagerId());
			ServiceReference serviceRef = deviceRef.getDevice().getServiceReference((URI) path.get(1), SecurityKey.EMPTY_KEY);
			Operation operation = serviceRef.getService().getOperation(null , new QName((String) path.get(2)).getLocalPart() , null, null);
			
			ParameterValue value = operation.invoke(operation.createInputValue(), CredentialInfo.EMPTY_CREDENTIAL_INFO);
			
			Iterator<Entry<Object, String>> entries = request.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry<Object, String> param = (Entry<Object, String>) entries.next();
				String result = ParameterValueManagement.getString(value, ((QName) param.getKey()).getLocalPart());
				
				if (((QName) param.getKey()).getLocalPart().equals("unit"))
				{
					result = UnitTable.dpws2gw.get(result).toString();
				}
				
				request.put(param.getKey(), result);
				gateway.setLastValueOfParameter(path, param.getKey(), param.getValue());
			}
			
			return request;
		}
		
		public void writeValue(RootEntity rootEnt, LinkedList<Object> path, Hashtable<Object, String> request) throws CommunicationException, AuthorizationException, InvocationException
		{
			AttributedURI uri = (AttributedURI) rootEnt.getOriginIdentifier();
			EndpointReference deviceEpr = new EndpointReference(uri);
			DeviceReference deviceRef = this.getDeviceReference(deviceEpr, comMan.getCommunicationManagerId());
			ServiceReference serviceRef = deviceRef.getDevice().getServiceReference((URI) path.get(1), SecurityKey.EMPTY_KEY);
			Operation operation = serviceRef.getService().getOperation(null , new QName((String) path.get(2)).getLocalPart() , null, null);
			
			ParameterValue inputValue = operation.createInputValue();
			
			if (inputValue.getMinOccurs() > 0)
			{
				if (inputValue.getType().isComplexType())
				{
					ComplexType type = (ComplexType) inputValue.getType();
					org.ws4d.java.structures.Iterator elements = type.elements();
					
					while ( elements.hasNext())
					{
						Element element = (Element) elements.next();
						
						if (element.getMinOccurs() > 0)
						{
							ParameterValueManagement.setString(inputValue, element.getName().getLocalPart(), gateway.getLastValueOfParameter(path, element.getName()));
						}
					}
				}
			}
			
			
			Iterator<Entry<Object, String>> entries = request.entrySet().iterator();
			while (entries.hasNext())
			{
				Map.Entry<Object, String> param = (Entry<Object, String>) entries.next();
				
				String val = null;
				if (((QName) param.getKey()).getLocalPart().equals("unit"))
				{
					val = UnitTable.gw2dpws.get(GWunit.valueOf(param.getValue())).toString();
				}else
				{
					val = param.getValue();
				}
				
				ParameterValueManagement.setString(inputValue, ((QName) param.getKey()).getLocalPart(), val);
				gateway.setLastValueOfParameter(path, param.getKey(), val);
			}

			operation.invoke(inputValue, CredentialInfo.EMPTY_CREDENTIAL_INFO);
		}
	
		public void subscribe(RootEntity rootEnt, LinkedList<Object> path, long duration) throws CommunicationException, EventingException, IOException
		{
			AttributedURI uri = (AttributedURI) rootEnt.getOriginIdentifier();
			EndpointReference deviceEpr = new EndpointReference(uri);
			DeviceReference deviceRef = this.getDeviceReference(deviceEpr, comMan.getCommunicationManagerId());
			ServiceReference serviceRef = deviceRef.getDevice().getServiceReference((URI) path.get(1), SecurityKey.EMPTY_KEY);
			EventSource eventSource = serviceRef.getService().getEventSource(null , new QName((String) path.get(2)).getLocalPart() , null, null);
			
			 
			ClientSubscription notificationSub = eventSource.subscribe(this, duration, CredentialInfo.EMPTY_CREDENTIAL_INFO);
			subscriptions.put(notificationSub.getClientSubscriptionId(), new EventSubscription(notificationSub, path, null));
		}
	
		
		private class EventSubscription
		{
			private ClientSubscription	notificationSub;
			private LinkedList<Object> path;
			private LinkedList<ParameterEntity> parameter;
			
			public EventSubscription(ClientSubscription	sub, LinkedList<Object> path, LinkedList<ParameterEntity> param)
			{
				this.notificationSub = sub;
				this.path = path;
				this.parameter = param;
			}
			
			public ClientSubscription getNotificationSub()
			{
				return this.notificationSub;
			}
			
			public LinkedList<Object> getPath()
			{
				return this.path;
			}
			
			public LinkedList<ParameterEntity> getParameter()
			{
				return this.parameter;
			}
		}
	}
	
	private class EntityOfComplexType
	{
		public EntityOfComplexType(SubEntity ent, ComplexType type, Direction direction, ParameterValue value)
		{
			org.ws4d.java.structures.Iterator elements = type.elements();
			Element element = null;
			
			while ( elements.hasNext())
			{
				element = (Element) elements.next();
				
				if (element.getType().isComplexType())
				{
					ent.addSubEntity(element.getName().getLocalPart(), element.getName());
					new EntityOfComplexType(ent.getSubEntity(element.getName()), (ComplexType) element.getType(), direction, value);
				} 
				else
				{
					ent.addParameter(element.getName().getLocalPart(), element.getName());
					ent.getParameter(element.getName()).setParameterName(ParameterTable.dpws2gw.get(element.getName().getLocalPart()));
					
					ent.getParameter(element.getName()).setDatatype(DatatypeTable.dpws2gw.get(SchemaUtil.getSchemaType(element.getType().getName())));
					if((ParameterTable.dpws2gw.get(element.getName().getLocalPart()) != null))
						if ((ParameterTable.dpws2gw.get(element.getName().getLocalPart()).equals(GWparameter.PRESVAL_UNIT)))
							ent.getParameter(element.getName()).setDatatype(GWdatatype.Unit);
					
					if (direction == Direction.out)
					{
						ent.getParameter(element.getName()).setReadable(true);
						ent.getParameter(element.getName()).setLastValue(ParameterValueManagement.getString(value, element.getName().getLocalPart()));
					}
					else if (direction == Direction.in)
					{
						ent.getParameter(element.getName()).setWriteable(true);
						ent.getParameter(element.getName()).setLastValue(DPWSHelper.getDefaultValue(ent.getParameter(element.getName()).getDatatype()));
					}
					else if(direction == Direction.event)
					{
						ent.getParameter(element.getName()).setSubscribable(true);
					}
				}
			}
		}
	}
	
	private enum Direction
	{
			in, out, event
	}

	private void makeLogFile()
	{
		FileHandler fh; 
		
		try {  
	        fh = new FileHandler("DPWSGate.log");  
	        log.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  

	    } catch (SecurityException | IOException e) {  
	        e.printStackTrace();  
	    }  
	}
}
