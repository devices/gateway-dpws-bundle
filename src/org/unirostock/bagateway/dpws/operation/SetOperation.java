/*
 * File: WriteOperation1.java
 * Project: org.unirostock.bagateway.dpws
 * @author Robert Balla
 */
package org.unirostock.bagateway.dpws.operation;

import java.util.Hashtable;
import java.util.LinkedList;

import org.unirostock.bagateway.dpws.DPWSHelper;
import org.unirostock.bagateway.dpws.tables.DatatypeTable;
import org.unirostock.bagateway.dpws.tables.UnitTable;
import org.unirostock.bagateway.entitiy.ParameterEntity;
import org.unirostock.bagateway.entitiy.SubEntity;
import org.unirostock.bagateway.enums.GWdatatype;
import org.unirostock.bagateway.interfaces.GatewayInterface;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

public class SetOperation extends Operation
{
	private GatewayInterface gateway;
	private LinkedList<Object> path;
	private LinkedList<ParameterEntity> parameter;
	private boolean hasInput;
	
	public SetOperation(String namespace, SubEntity ent, String operationName, String interfaceName, GatewayInterface gateway)
	{
		super("Set" + operationName, new QName(interfaceName, namespace));
		
		this.gateway = gateway;
		this.path = ent.getPath();
		this.parameter = new LinkedList<ParameterEntity>();
		this.hasInput = false;
		
		ComplexType subEntType = new ComplexType(DPWSHelper.adaptString(ent.getOriginIdentifier().toString()) + "SetType", namespace, ComplexType.CONTAINER_SEQUENCE);
		
		LinkedList<ParameterEntity> parameterList = ent.getAllParameter();
		
		for (ParameterEntity param : parameterList)
		{
			if(param.isWriteable())
			{
				this.parameter.add(param);
				Type type = DatatypeTable.gw2dpws.get(param.getDatatype());
				if (type == null)
					type = SchemaUtil.TYPE_STRING;
				
				Element paramElement = new Element(new QName(DPWSHelper.adaptString(param.getOriginIdentifier().toString()), namespace), type);
				paramElement.setMinOccurs(0);
				paramElement.setMaxOccurs(1);
				
				subEntType.addElement(paramElement);
				this.hasInput = true;
			}
		}
		
		Element value = new Element(new QName("Set" + DPWSHelper.adaptString(ent.getOriginIdentifier().toString()), namespace), subEntType);
		if (hasInput == true)
		{
			this.setInputName(DPWSHelper.adaptString("Set" + DPWSHelper.adaptString(ent.getOriginIdentifier().toString()) + "Input"));
			this.setInput(value);
		}
	}
	
	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException
	{
		Hashtable<Object, String> request = new Hashtable<Object, String>();
		
		for (ParameterEntity param : this.parameter)
		{
			if(param.isWriteable())
			{
				String value = ParameterValueManagement.getString(parameterValue, DPWSHelper.adaptString(param.getOriginIdentifier().toString()));
				if (value != null)
				{
					if (param.getDatatype().equals(GWdatatype.Unit))
					{
						value = UnitTable.dpws2gw.get(value).toString();
					}
					
					request.put(param.getOriginIdentifier(), value);
				}
			}
		}
		
		if (request.size() > 0)
			gateway.writeValue(path, request);
		
		return parameterValue;
	}
	
	public boolean hasInput()
	{
		return this.hasInput;
	}

}
