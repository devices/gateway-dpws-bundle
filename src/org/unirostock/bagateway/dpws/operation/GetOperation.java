/*
 * File: ReadOperation1.java
 * Project: org.unirostock.bagateway.dpws
 * @author Robert Balla
 */
package org.unirostock.bagateway.dpws.operation;

import java.util.Hashtable;
import java.util.LinkedList;
import java.lang.IllegalArgumentException;

import org.unirostock.bagateway.dpws.DPWSHelper;
import org.unirostock.bagateway.dpws.tables.DatatypeTable;
import org.unirostock.bagateway.dpws.tables.UnitTable;
import org.unirostock.bagateway.entitiy.ParameterEntity;
import org.unirostock.bagateway.entitiy.SubEntity;
import org.unirostock.bagateway.enums.GWdatatype;
import org.unirostock.bagateway.enums.GWunit;
import org.unirostock.bagateway.interfaces.GatewayInterface;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

public class GetOperation extends Operation
{
	private GatewayInterface gateway;
	private LinkedList<Object> path;
	private LinkedList<ParameterEntity> parameter;
	private boolean hasOutput;
	
	public GetOperation(String namespace, SubEntity ent, String operationName, String interfaceName, GatewayInterface gateway)
	{
		super("Get" + operationName, new QName(interfaceName, namespace));
		
		this.gateway = gateway;
		this.path = ent.getPath();
		this.parameter = new LinkedList<ParameterEntity>();
		this.hasOutput = false;
		
		ComplexType subEntType = new ComplexType(DPWSHelper.adaptString(ent.getOriginIdentifier().toString()) + "GetType", namespace, ComplexType.CONTAINER_SEQUENCE);
		
		LinkedList<ParameterEntity> parameterList = ent.getAllParameter();
		
		for (ParameterEntity param : parameterList)
		{
			if(param.isReadable())
			{
				this.parameter.add(param);
				Type type = DatatypeTable.gw2dpws.get(param.getDatatype());
				if (type == null)
					type = SchemaUtil.TYPE_STRING;
				
				subEntType.addElement(new Element(new QName(DPWSHelper.adaptString(param.getOriginIdentifier().toString()), namespace), type));
				hasOutput = true;
			}
		}
		
		Element value = new Element(new QName("Get" + DPWSHelper.adaptString(ent.getOriginIdentifier().toString()), namespace), subEntType);
		
		this.setOutputName(DPWSHelper.adaptString("Get" + DPWSHelper.adaptString(ent.getOriginIdentifier().toString()) + "Output"));
		this.setOutput(value);
	}
	
	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException
	{
		Hashtable<Object, String> request = new Hashtable<Object, String>();
		
		for (ParameterEntity param : this.parameter)
		{
			if(param.isReadable())
			{
				request.put(param.getOriginIdentifier(), "");
			}
		}
		
		request = gateway.readValue(path, request);
		
		ParameterValue result = createOutputValue();
		
		for (ParameterEntity param : parameter)
		{
			String value = null;
			GWdatatype type = param.getDatatype();
			if (type != null)
			{
				if (type.equals(GWdatatype.Unit))
				{
					GWunit unit = null;
					try
					{
						unit = GWunit.valueOf(request.get(param.getOriginIdentifier()));
					}catch (IllegalArgumentException e)
					{
						e.printStackTrace();
					}
						
					if (unit != null)
					{
						value = UnitTable.gw2dpws.get(unit).toString();
					}else
					{
						value = request.get(param.getOriginIdentifier());
					}
				}else
				{
					value = request.get(param.getOriginIdentifier());
				}
			}else
			{
				value = request.get(param.getOriginIdentifier());
			}
			
			ParameterValueManagement.setString(result, DPWSHelper.adaptString(param.getOriginIdentifier().toString()), value);
		}
	
		return result;
	}
	
	public boolean hasOutput()
	{
		return this.hasOutput;
	}
	
}
