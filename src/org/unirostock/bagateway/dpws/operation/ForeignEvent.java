package org.unirostock.bagateway.dpws.operation;

import java.util.Hashtable;
import java.util.LinkedList;

import org.unirostock.bagateway.dpws.DPWSHelper;
import org.unirostock.bagateway.dpws.tables.DatatypeTable;
import org.unirostock.bagateway.dpws.tables.UnitTable;
import org.unirostock.bagateway.entitiy.ParameterEntity;
import org.unirostock.bagateway.entitiy.SubEntity;
import org.unirostock.bagateway.enums.GWdatatype;
import org.unirostock.bagateway.enums.GWunit;
import org.unirostock.bagateway.interfaces.GatewayInterface;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.DefaultEventSource;
import org.ws4d.java.service.ServiceSubscription;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

public class ForeignEvent extends DefaultEventSource
{
	private GatewayInterface gateway;
	private LinkedList<Object> path;
	private LinkedList<ParameterEntity> parameter;
	private boolean hasOutput;
	
	public ForeignEvent(String namespace, SubEntity ent, String operationName, String interfaceName, GatewayInterface gateway)
	{
		super("Subscribe" + operationName, new QName(interfaceName, namespace));
		
		this.gateway = gateway;
		this.path = ent.getPath();
		this.parameter = new LinkedList<ParameterEntity>();
		this.hasOutput = false;
		
		this.gateway = gateway;
		this.path = ent.getPath();
		this.parameter = new LinkedList<ParameterEntity>();
		this.hasOutput = false;
		
		ComplexType subEntType = new ComplexType(DPWSHelper.adaptString(ent.getOriginIdentifier().toString()) + "SubscritionType", namespace, ComplexType.CONTAINER_SEQUENCE);
		
		LinkedList<ParameterEntity> parameterList = ent.getAllParameter();
		
		for (ParameterEntity param : parameterList)
		{
			if(param.isSubscribable())
			{
				this.parameter.add(param);
				Type type = DatatypeTable.gw2dpws.get(param.getDatatype());
				if (type == null)
					type = SchemaUtil.TYPE_STRING;
				
				subEntType.addElement(new Element(new QName(DPWSHelper.adaptString(param.getOriginIdentifier().toString()), namespace), type));
				hasOutput = true;
			}
		}
		
		Element value = new Element(new QName("Subscribe" + DPWSHelper.adaptString(ent.getOriginIdentifier().toString()), namespace), subEntType);
		this.setOutputName("Subscribe" + DPWSHelper.adaptString(ent.getOriginIdentifier().toString()));
		setOutput(value);
	}
	
	public void fireForeignEvent(Hashtable<Object, String> values)
	{
		ParameterValue result = createOutputValue();
		
		for (ParameterEntity param : parameter)
		{
			String value = null;
			GWdatatype type = param.getDatatype();
			if (type != null)
			{
				if (type.equals(GWdatatype.Unit))
				{
					GWunit unit = null;
					try
					{
						unit = GWunit.valueOf(values.get(param.getOriginIdentifier()));
					}catch (IllegalArgumentException e)
					{
						e.printStackTrace();
					}
						
					if (unit != null)
					{
						value = UnitTable.gw2dpws.get(unit).toString();
					}else
					{
						value = values.get(param.getOriginIdentifier());
					}
				}else
				{
					value = values.get(param.getOriginIdentifier());
				}
			}else
			{
				value = values.get(param.getOriginIdentifier());
			}
			
			ParameterValueManagement.setString(result, DPWSHelper.adaptString(param.getOriginIdentifier().toString()), value);
		}
		
		this.fire(result, 0, CredentialInfo.EMPTY_CREDENTIAL_INFO);
	}
	
	public boolean hasOutput()
	{
		return this.hasOutput;
	}
	
	@Override
	protected void addSubscription(ServiceSubscription subscription) {
		this.subscriptions.exclusiveLock();
		try {
			subscriptions.add(subscription);
		} finally {
			subscriptions.releaseExclusiveLock();
		}
		
		gateway.subscribe(path, subscription.getExpirationTime() - System.currentTimeMillis());
	}
}
