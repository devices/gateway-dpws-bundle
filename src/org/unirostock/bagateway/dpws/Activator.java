/*
 * File: Activator.java
 * Project: org.unirostock.bagateway.dpws
 * @author Robert Balla
 */
package org.unirostock.bagateway.dpws;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.unirostock.bagateway.interfaces.GateInterface;
import org.unirostock.bagateway.interfaces.GatewayInterface;

// TODO: Auto-generated Javadoc
/**
 * The Class Activator.
 */
public class Activator implements BundleActivator {

	/** The context. */
	private static BundleContext context;
	
	/** The gate reg. */
	private ServiceRegistration<?> gateReg;
	
	/** The dpws gate. */
	private DPWSGateImpl dpwsGate;
	
	/** The gateway ref. */
	private ServiceReference<GatewayInterface> gatewayRef;

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception
	{
		Activator.context = bundleContext;
		System.out.println("Start DPWS-Gate");
		
		gateReg = context.registerService(GateInterface.class.getName(), new DPWSGateImpl(), null);
		gatewayRef = context.getServiceReference(GatewayInterface.class);
		dpwsGate = (DPWSGateImpl) context.getService(gateReg.getReference());
		dpwsGate.setGateway(context.getService(gatewayRef));
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception
	{
		dpwsGate.closeGate();
		context.ungetService(gatewayRef);
		gateReg.unregister();
		Activator.context = null;
	}

}