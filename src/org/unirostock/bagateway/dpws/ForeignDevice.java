/*
 * File: NoDPWSDevice.java
 * Project: org.unirostock.bagateway.dpws
 * @author Robert Balla
 */
package org.unirostock.bagateway.dpws;

import org.unirostock.bagateway.dpws.service.ForeignService;
import org.unirostock.bagateway.entitiy.RootEntity;
import org.unirostock.bagateway.interfaces.GatewayInterface;
import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.ConnectionInfo;
import org.ws4d.java.message.discovery.ProbeMatch;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;

// TODO: Auto-generated Javadoc
/**
 * The Class NoDPWSDevice.
 */
public class ForeignDevice extends DefaultDevice
{
	/**
	 * Instantiates a new no dpws device.
	 *
	 * @param comManId the com man id
	 * @param ent the ent
	 * @param gw the gw
	 */
	public ForeignDevice(String comManId, RootEntity ent, GatewayInterface gw)
	{
		super(comManId);
		
		GatewayInterface gateway = gw;
		
		String deviceNamespace = DPWSHelper.adaptString(DPWSGateImpl.NAMESPACE + "/" + ent.getOriginProtocol() + "/" + ent.getName());
				
		this.setPortTypes(new QNameSet(new QName(ent.getName(), deviceNamespace)));
		this.addFriendlyName(LocalizedString.LANGUAGE_DE, DPWSHelper.adaptString(ent.getName()));
		this.addFriendlyName(LocalizedString.LANGUAGE_EN, DPWSHelper.adaptString(ent.getName()));
		
		String serviceNamespace = DPWSHelper.adaptString(deviceNamespace + "/" + ent.getName() + "Service");
		this.addService(new ForeignService(comManId, ent, serviceNamespace, gateway));	
	}
	
	/**
	 * Send propbe matches.
	 *
	 * @param msg the msg
	 * @param connectInfo the connect info
	 * @param comMan the com man
	 */
	public void sendPropbeMatch(ProbeMessage msg, ConnectionInfo connectInfo, CommunicationManager comMan)
	{
		ProbeMatchesMessage res = new ProbeMatchesMessage();
		res.setResponseTo(msg);
		res.getHeader().setAppSequence(this.appSequencer.getNext());
		
		ProbeMatch pm = new ProbeMatch();
		pm.setEndpointReference(this.getEndpointReference());
		pm.setMetadataVersion(this.getMetadataVersion());
		
		res.addProbeMatch(pm);
		comMan.send(res, connectInfo.getRemoteXAddress(), connectInfo.getLocalCredentialInfo());		
	}
}
