/*
 * File: DPWSGateService.java
 * Project: org.unirostock.bagateway.dpws
 * @author Robert Balla
 */
package org.unirostock.bagateway.dpws.service;

import java.util.List;

import org.unirostock.bagateway.dpws.DPWSGateImpl;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.types.URI;


public class GateService extends DefaultService
{

	public final static URI	DPWSGATE_SERVICE_ID	= new URI(DPWSGateImpl.NAMESPACE + "/DPWSGateService");
	
	public GateService(String communicationManagerId)
	{
		super(communicationManagerId);
		
		this.setServiceId(DPWSGATE_SERVICE_ID);
		

	}

}
