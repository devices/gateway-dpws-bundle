/*
 * File: NoDPWSService1.java
 * Project: org.unirostock.bagateway.dpws
 * @author Robert Balla
 */
package org.unirostock.bagateway.dpws.service;

import java.util.LinkedList;

import org.unirostock.bagateway.dpws.DPWSHelper;
import org.unirostock.bagateway.dpws.operation.ForeignEvent;
import org.unirostock.bagateway.dpws.operation.GetOperation;
import org.unirostock.bagateway.dpws.operation.SetOperation;
import org.unirostock.bagateway.entitiy.RootEntity;
import org.unirostock.bagateway.entitiy.SubEntity;
import org.unirostock.bagateway.interfaces.GatewayInterface;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.types.URI;

public class ForeignService extends DefaultService
{
	public ForeignService(String comManId, RootEntity ent, String namespace, GatewayInterface gateway)
	{
		super(comManId);
		
		this.setServiceId(new URI(namespace));
		
		LinkedList<SubEntity> subEntities = ent.getAllSubEntities();
		
		for(SubEntity subEnt : subEntities)
		{
			this.newOperation(subEnt, namespace, gateway);
		}
	}
	
	private void newOperation(SubEntity ent, String namespace, GatewayInterface gateway)
	{	
		if (ent.getAllParameter() != null)
		{
			String operationName =  null;
			if (ent.getDirection() != null && ent.getCount() != -1)
			{
				operationName =  DPWSHelper.adaptString(ent.getDirection().toString() + ent.getCount().toString());
			}
			else
			{
				operationName =  DPWSHelper.adaptString(ent.getOriginIdentifier().toString());
			}
			
			
			String interfaceName = null;
			if (ent.getGroup() != null)
			{
				interfaceName = DPWSHelper.adaptString(ent.getGroup().toString());
			}
			else
			{
				interfaceName =  DPWSHelper.adaptString(ent.getOriginIdentifier().toString());
			}
			
			GetOperation getOp = new GetOperation(namespace, ent, operationName, interfaceName, gateway);
			if (getOp.hasOutput())
				this.addOperation(getOp);
			
			SetOperation setOp = new SetOperation(namespace, ent, operationName, interfaceName, gateway);
			if (setOp.hasInput())
				this.addOperation(setOp);
			
			ForeignEvent event = new ForeignEvent(namespace, ent, operationName, interfaceName, gateway);
			if (event.hasOutput())
				this.addEventSource(event);
		}
		
		LinkedList<SubEntity> subEntities = ent.getAllSubEntities();
		if (subEntities != null)
		{
			for(SubEntity subEnt : subEntities)
			{
				this.newOperation(subEnt, namespace, gateway);
			}
		}
	}
}
